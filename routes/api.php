<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

Route::get('/', function() {
    return response()->json(["status" => "Welcome to Raposa's API"]);
});

// USERS MICROSERVICE
Route::post('/users/login', 'UserController@login');
Route::post('/users/register', 'UserController@register');
Route::get('/users/logout', 'UserController@logout')->middleware('auth');
Route::put('users/update', 'UserController@updateUser')->middleware('auth');

Route::delete('users/delete', 'UserController@deleteUser')->middleware('auth', 'admin');

// MENUS MICROSERVICE
Route::get('/menus/all', 'MenuController@getMenu');
Route::get('/menus/id/{id}', 'MenuController@getMenuById');
Route::get('/menus/category/{category}', 'MenuController@getMenuByCategory');

Route::post('menus/add', 'MenuController@addMenu')->middleware('auth', 'admin');
Route::put('menus/update', 'MenuController@updateMenu')->middleware('auth', 'admin');
Route::delete('menus/delete', 'MenuController@deleteMenu')->middleware('auth', 'admin');

// SEATS MICROSERVICE
Route::get('/seats/all', 'SeatController@getSeatsAll');
Route::get('/seats/all/{branch}', 'SeatController@getSeatsBranch');
Route::get('/seats/id/{id}', 'SeatController@getSeatsId');
Route::put('/seats/updatestatus', 'SeatController@updateSeatStatus');

Route::post('/seats/add', 'SeatController@addSeat')->middleware('auth', 'admin');
Route::put('/seats/update', 'SeatController@updateSeat')->middleware('auth', 'admin');
Route::delete('/seats/delete', 'SeatController@deleteSeat')->middleware('auth', 'admin');

// ORDERS MICROSERVICE

// TODO EXPORT SQL






