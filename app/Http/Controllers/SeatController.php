<?php

namespace App\Http\Controllers;

use App\Seat;
use Illuminate\Http\Request;

class SeatController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    function responseText($text, $status)
    {
        return response()->json([
            "status" => $text
        ], $status);
    }

    function getSeatsAll()
    {

        $seat = new Seat;

        return $seat->get();
    }

    function getSeatsBranch($branch)
    {

        $seat = new Seat;
        $theseat = $seat->where('branch', $branch)->get();

        if ($theseat) {
            return $theseat;
        }
        return $this->responseText("branch does not exist", 400);
    }

    function getSeatsId($id)
    {

        $seat = new Seat;
        $theseat = $seat->find($id);

        if ($theseat) {
            return $theseat;
        }
        return $this->responseText("seat id does not exist", 400);
    }

    function updateSeatStatus(Request $request)
    {

        $seat = new Seat;

        $id = $request->id;
        $status = $request->status;

        $theseat = $seat->find($id);

        if ($theseat) {
            if ($status) {
                if ($status == "empty" || $status == "unavailable" || $status == "occupied") {
                    $theseat->status = $status;
                    if ($theseat->save()) {
                        return $this->responseText("successful update of seat status", 200);
                    }
                    return $this->responseText("unable to update seat status", 400);
                }
                return $this->responseText("supported seat status include: ['empty', 'unavailable', 'occupied']", 400);
            }
            return $this->responseText("please provide new seat status", 400);
        }
        return $this->responseText("please provide seat id to update", 400);
    }

    function addSeat(Request $request)
    {
        $seat = new Seat;

        $branch_re = $request->branch;
        $seat_number_re = $request->seat_number;
        $status_re = $request->status;

        if ($branch_re && $seat_number_re && $status_re) {

            if ($status_re == "empty" || $status_re == "occupied" || $status_re == "unavailable") {
                $seat->branch = $branch_re;
                $seat->seat_number = $seat_number_re;
                $seat->status = $status_re;

                if ($seat->save()) {
                    return $this->responseText("seat successfully saved", 200);
                }
                return $this->responseText("unable to save seat", 400);
            }
            return $this->responseText("supported seat status include: ['empty', 'unavailable', 'occupied']", 400);
        }
        return $this->responseText("please provide parameters", 400);
    }
}
