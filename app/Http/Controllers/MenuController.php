<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    function responseText($text, $status)
    {
        return response()->json([
            "status" => $text
        ], $status);
    }

    function getMenu()
    {

        $menu = new Menu;

        if (!$menu->get()->isEmpty()) {
            return $menu->get();
        }
        return 'menu is empty';
    }

    function getMenuById($id)
    {

        $menu = new Menu;
        $themenu = $menu->find($id);

        if ($themenu) {
            return $themenu;
        }
        return 'menu does not exist';
    }

    function getMenuByCategory($category)
    {

        $menu = new Menu;
        $themenu = $menu->where('category', $category);

        if (!$themenu->get()->isEmpty()) {
            return $themenu->get();
        }
        return $this->responseText("category does not exist", 400);
    }

    function addMenu(Request $request)
    {
        $menu = new Menu;

        $name_re = $request->name;
        $category_re = $request->category;
        $description_re = $request->description;
        $price_re = $request->price;

        if ($name_re && $category_re && $description_re && $price_re) {

            $menu->name = $name_re;
            $menu->category = $category_re;
            $menu->description = $description_re;
            $menu->price = $price_re;

            if ($menu->save()) {
                return $this->responseText("Successful addition of dish", 200);
            }
            return $this->responseText("Unable to add dish", 400);
        }

        return $this->responseText("please provide all essential dish details", 400);
    }

    function updateMenu(Request $request)
    {

        $menu = new Menu;

        $id = $request->id;

        if ($id) {

            $themenu = $menu->find($id);

            if ($themenu) {
                $name_re = $request->name;
                $category_re = $request->category;
                $description_re = $request->description;
                $price_re = $request->price;

                if ($name_re || $category_re || $description_re || $price_re) {
                    if ($name_re) {
                        $themenu->name = $name_re;
                    }
                    if ($category_re) {
                        $themenu->category = $category_re;
                    }
                    if ($description_re) {
                        $themenu->description = $description_re;
                    }
                    if ($price_re) {
                        $themenu->price = $price_re;
                    }

                    if ($themenu->save()) {
                        return $this->responseText("menu update successful", 200);
                    }
                    return $this->responseText("unable to update menu", 400);
                }
                return $this->responseText("please provide parameters to update", 400);
            }
            return $this->responseText("menu does not exist", 400);
        }
        return $this->responseText("please provide menu id to update", 400);
    }

    function deleteMenu(Request $request)
    {
        $menu = new Menu;

        $id = $request->id;

        if ($id) {
            $themenu = $menu->find($id);

            if ($themenu) {
                if ($themenu->delete()) {
                    return $this->responseText("menu deletion successful", 200);
                }
                return $this->responseText("unable to delete menu", 400);
            }
            return $this->responseText("menu does not exist", 400);
        }
        return $this->responseText("please provide menu id to update", 400);
    }
}
